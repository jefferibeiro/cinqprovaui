import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AcompanhamentoService {

  constructor(protected httpClient: HttpClient) { }

  public listarPosicoes(): Observable<any> {
    return this.httpClient.get(`http://localhost:8080/posicoes/listartodos`);
  }
}
