import { TestBed } from '@angular/core/testing';

import { AcompanhamentoService } from './acompanhamento.service';

describe('AcompanhamentoService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AcompanhamentoService = TestBed.get(AcompanhamentoService);
    expect(service).toBeTruthy();
  });
});
