import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {TableModule} from 'primeng/table';
import {ButtonModule} from 'primeng/button';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TelaAcompanhamentoComponent } from './tela-acompanhamento/tela-acompanhamento.component';
import { AcompanhamentoService } from 'src/services/acompanhamento.service';
import {DialogModule} from 'primeng/dialog';
import { AgmCoreModule } from '@agm/core';

@NgModule({
  declarations: [
    AppComponent,
    TelaAcompanhamentoComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    TableModule,
    ButtonModule,
    HttpClientModule,
    DialogModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyBbBlLDNE7Bc6zqdbcR0qQ1W-7Z99cVZW8'
    })
  ],
  providers: [AcompanhamentoService],
  bootstrap: [AppComponent]
})
export class AppModule { }
