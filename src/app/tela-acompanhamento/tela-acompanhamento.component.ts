import { Component, OnInit } from '@angular/core';
import { AcompanhamentoService } from 'src/services/acompanhamento.service';

declare var google: any;

@Component({
  selector: 'app-tela-acompanhamento',
  templateUrl: './tela-acompanhamento.component.html',
  styleUrls: ['./tela-acompanhamento.component.css']
})
export class TelaAcompanhamentoComponent implements OnInit {
  posicoes=[];
  teste=[];
  cols: any[];
  display: boolean = false;
  options: any;
  lat = '';
  lng = '';
  placaSelecionado='';
  posicaoSelecionado='';
  dataposicaoSelecionado = '';
  ignicaoSelecionado = '';
  velocidadeSelecionado = '';
  hora ='';


  constructor(private acompanhamentoService: AcompanhamentoService) { }

  ngOnInit() {
    this.acompanhamentoService.listarPosicoes().subscribe(x=>
    {
      this.posicoes = x;
    });

    this.cols = [
      { field: 'placa', header: 'Placa' },
      { field: 'dataFormatada', header: 'Data' },
      { field: 'velocidade', header: 'Velocidade' },
      { field: 'posicao', header: 'Posição' },
      { field: 'ignicaoStatus', header: 'Ignição' }
    ];
  }

  onClickMap(event: any){
    console.log(event)
    this.display = true
    this.placaSelecionado= event.placa;
    this.posicaoSelecionado= event.posicao;
    this.dataposicaoSelecionado = event.dataFormatada;
    this.ignicaoSelecionado = event.ignicaoStatus;
    this.velocidadeSelecionado = event.velocidade;
    this.lat = event.latitude;
    this.lng = event.longitude;
    this.hora = event.horaFormatada;
  }
}
