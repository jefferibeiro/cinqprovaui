import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TelaAcompanhamentoComponent } from './tela-acompanhamento.component';

describe('TelaAcompanhamentoComponent', () => {
  let component: TelaAcompanhamentoComponent;
  let fixture: ComponentFixture<TelaAcompanhamentoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TelaAcompanhamentoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TelaAcompanhamentoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
